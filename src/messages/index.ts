// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// import { de } from './de';
// Falls nur das Backend benutzt werden soll, bitte "translation: {}" benutzen, ansonsten "translation: de"
export const messages = {
  de: {
    translation: {},
  },
};
