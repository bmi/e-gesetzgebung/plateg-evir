// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/**
 *
 * ! THIS FILE IS NOT BEEING USED, TRANSLATION COMES FROM BE !
 *
 */
export const de = {
  evir: {
    header: {
      linkHome: 'eViR',
      linkGlossar: 'Glossar',
    },
    glossar: {
      drawerTitle: 'eViR - Glossar',
      drawerText: 'Evir text',
    },
    home: {
      title: 'Elektronischer Verfahrensassistent im Rechtsetzungsprozess',
      titleVerfahrensassistent: 'Der Verfahrensassistent',
      text1:
        'Dieser Verfahrensassistent im Rechtsetzungsprozess (eViR) begleitet Sie Schritt für Schritt durch das Rechtsetzungsverfahren.',
      text2:
        'Er bietet Informationen zu Gesetzesinitiativen von Bundesregierung, Bundestag und Bundesrat sowie zu Rechtsverordnungen und allgemeinen Verwaltungsvorschriften. In der Praxis können die Schritte nicht immer in einer logischen und zeitlichen Reihenfolge durchgeführt werden. Einige werden im konkreten Arbeitsprozess parallel ausgeführt, andere müssen mehrfach wiederholt werden, weil sich möglicherweise Umstände oder Erkenntnisse im Laufe des Verfahrens verändern. Mit solchen Unwägbarkeiten, die den Prozess verlängern, muss gerechnet werden. Zudem sind bei einzelnen Schritten bestimmte Fristen einzuhalten. Eine gute Zeitplanung gehört daher zu den wichtigsten Erfolgsfaktoren des Rechtssetzungsverfahrens.',
      titleVerfahrensassistentGesetze: 'Verfahrensassistent für Gesetze:',
      linkRegierungsinitiative: 'Regierungsinitiative',
      titleVerfahrensassistentRechtsverordnungen: 'Verfahrensassistent für Rechtsverordnungen:',
      linkRechtsverordnung: 'Rechtsverordnung der Bundesregierung oder eines oder mehrerer Bundesminister',
      titleVerfahrensassistentVerwaltungsvorschriften: 'Verfahrensassistent für Verwaltungsvorschriften:',
      linkAllgemeineVerwaltungsvorschrift: 'Allgemeine Verwaltungsvorschrift',
    },
    modules: {
      evor: { title: 'eVoR', description: 'Elektronische Vorbereitung von Regelungsentwürfen', link: '#/evor' },
      zeit: { title: 'ZEIT', description: 'Elektronische Zeitplanung', link: '#/zeit' },
      editor: { title: 'Editor', description: 'Editor der E-Gesetzgebung', link: '#/editor' },
      hra: { title: 'Abstimmung', description: 'Abstimmung von Regelungsentwürfen', link: '#/hra' },
      pkp: { title: 'PKP', description: 'Planungs- und Kabinettsmanagement-Programm', link: '#/pkp' },
      rv: { title: 'RV', description: 'Regelungsvorhaben', link: '#/regelungsvorhaben' },
      egfa: { title: 'eGFA', description: 'Elektronische Gesetzesfolgenabschätzung', link: '#/egfa' },
      bib: { title: 'BIB', description: 'Arbeitshilfenbibliothek', link: '#/hilfen' },
    },
    marginalSpalte: {
      applicationsTitle: 'Relevante Anwendungen',
      bibEntriesTitle: 'Relevante Arbeitshilfen',
    },
    nav: {
      gesetz: {
        nameLink: 'Gesetz',
        regierungsinitiative: {
          nameLink: 'Regierungsinitiative',
          content: `<p>Nach Artikel 76 Absatz 1 GG können Gesetzentwürfe von der Bundesregierung, aus der „Mitte des Bundestages“ (dies sind mindestens 5 % der Mitglieder des Bundestages oder eine Fraktion) oder durch den Bundesrat in den Bundestag eingebracht werden. Den Kreis der Gesetzesinitianten regelt Artikel 76 Absatz 1 GG abschließend.</p>
                          <p>In der Staatspraxis überwiegen die Initiativen der Bundesregierung. Der hohe Anteil der Gesetzesinitiativen seitens der Bundesregierung ist dadurch begründet, dass die Bundesregierung die operative Gestaltung der Bundespolitik prägt und über die notwendigen fachlichen und personellen Ressourcen verfügt (Ministerialverwaltung, nachgeordnete Behörden).</p>`,
          phaseTitle1: {
            nameLink: 'Phase 1 <br>Erstellung eines Gesetzenwurfs durch die Bundesregierung',
            vorueberlegungen11: {
              nameLink: '1.1 Vorüberlegungen',
              content: `<p>Die Federführung für die Erarbeitung eines Gesetzentwurfs liegt bei dem Ressort, dem das Sachgebiet nach der Geschäftsverteilung der Bundesregierung zugewiesen ist. Die Geschäftsverteilung erfolgt durch Organisationserlass des Bundeskanzlers.</p><p>Das federführende Ressort – und dort die zuständige Organisationseinheit, die mit der Durchführung des Gesetzgebungsverfahrens beauftragt ist – trägt die Gesamtverantwortung für die fachlichen Belange und die Einhaltung eines ordnungsgemäßen Verfahrens. Das Verfahren richtet sich nach der GGO.</p><p>Im Regelfall findet im Prozess der Erarbeitung des Regierungsentwurfs eine Rückkopplung zwischen der zuständigen Organisationseinheit und der Leitung des Ressorts statt. So kann beispielsweise eine Leitungsvorlage zur Notwendigkeit einer gesetzlichen Neuregelung und zu deren Zielen zu Eckpunkten des Vorhabens oder zu beabsichtigten Konsultationen notwendig sein. Eine Ausnahme von diesem Regelfall kann z.B. eine alternativlose Umsetzung von europarechtlichen Vorgaben in nationales Recht sein.</p><p>Eine Abstimmung des Gesetzentwurfs mit anderen Ministerien muss erfolgen, falls deren Belange betroffen sind.</p><h3><span style="letter-spacing: 0.16px;">Anlässe für ein Tätigwerden des federführenden Bundesministeriums</span></h3><p>Anlässe für ein Tätigwerden des federführenden Bundesministeriums können sich insbesondere ergeben durch:</p><ul><li>politische Weisung der Hausleitung</li><li>Beschluss der gesetzgebenden Körperschaften oder Zusagen der Bundesregierung ihnen gegenüber</li><li>Umsetzung des Regierungsprogramms oder einer Koalitionsvereinbarung</li><li>Gerichtsentscheidung</li><li>bevorstehendes Auslaufen vorhandener gesetzlicher Regelungen (befristete Gesetze)</li><li>Evaluierung des geltenden Rechts/retrospektive Gesetzesfolgenabschätzung (rGFA)</li><li>EU-Recht</li><li>Abschluss völkerrechtlicher Verträge (Vertragsgesetz und ggf. erforderliche Umsetzung in nationales Recht)</li><li>Aktivität der parlamentarischen Opposition</li><li>Vorschlag der Länder</li><li>Wunsch der Gesetzesanwender (z.B. Fachkreise und Verbände)</li><li>Vorschlag der Wissenschaft</li><li>Notwendigkeit aus eigener fachlicher Sicht</li><li>Petition</li><li>Bemerkungen/Empfehlungen/Forderungen etc. des Bundesrechnungshofes.</li></ul>`,
              linkedBibEntries: [
                'AH Experimentierklauseln',
                'EU-Handbuch',
                'GGO',
                'Leitfaden vV',
                'RiVeVo',
                'RvV',
                'Zusammenarbeit EU',
              ],
              linkedModules: ['zeit', 'rv'],
              pruefungregelungsnotwendigkeit111: {
                nameLink: '1.1.1 Prüfung: Regelungsnotwendigkeit',
                content: `<p>Zunächst wird in der Regel gefragt, ob ein Gesetz erforderlich ist (siehe <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 43 Absatz 1 Nummer 1 GGO</a>). Zur Feststellung der Erforderlichkeit sind verschiedene Aspekte zu beachten.</p><ol><li>Es muss geklärt werden, ob eine Regelung überhaupt notwendig ist. Es ist zu prüfen, ob für eine Neuregelung eine hinreichende Tatsachengrundlage gegeben ist und ob sich Ziele oder eine Problembewältigung auch anders - z.B. durch alternative Formen der Problembewältigung (z.B. Selbstregulierung) oder problemorientierte Auslegung/Anwendung der vorhandenen rechtlichen Regelungen - erreichen lassen. Eine Hilfestellung hierzu bietet die <a href="/#/evor">elektronische Vorbereitung von Regelungsentwürfen (eVoR)</a>. <br>Für die Klärung der Frage, ob ein festgestellter Regelungsbedarf statt durch hoheitliche Regulierung eher durch Selbstregulierung gedeckt werden sollte, finden Sie im „<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">Prüfkatalog zur Feststellung von Selbstregulierungsmöglichkeiten</a>" konkrete Hilfestellungen. Entscheidendes Kriterium für die Beantwortung der Fragen ist, ob sich die jeweils „schärfere" Form der Regulierung als notwendig i.S.d. <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 43 Absatz 1 Nummer 1 GGO</a> erweist.</li><li>Wenn die Notwendigkeit einer neuen Regelung bejaht wird, stellt sich die Frage, ob diese Regelung durch ein förmliches Gesetz erfolgen muss. Eine Regelung durch ein förmliches Gesetz ist nötig,<br>a. wenn zwingende verfassungsrechtliche Vorgaben vorliegen<br>b. wenn die änderungsbedürftige Rechtslage ihrerseits auf einem förmlichen Gesetz beruht. Insbesondere in Fällen, in denen eine Materie durch eine Kombination von gesetzlichen und untergesetzlichen Vorschriften ausgestaltet worden ist. muss daher sorgfältig geprüft werden, auf welcher Ebene der festgestellte Regelungsbedarf genau anzusiedeln ist.</li><li>Wenn die rechtliche Prüfung der Notwendigkeit einer neuen gesetzlichen Regelung negativ ausfällt, ist aus fachlicher und politischer Sicht zu fragen, ob eine Problemlösung durch den Erlass eines förmlichen Gesetzes dennoch geboten ist. Dies ist insbesondere dann anzunehmen, wenn eine Angelegenheit umstritten und deshalb die besondere Legitimation der entsprechenden Regelung angezeigt erscheint. Für den Erlass untergesetzlicher Normen sprechen dagegen deren Verfahrensökonomie und höhere Flexibilität bei etwaigem späterem Änderungsbedarf.<br></li><li>Soweit der festgestellte Regelungsbedarf nicht aus zwingenden rechtlichen Gründen durch ein förmliches Gesetz gedeckt werden muss, ist stets zu prüfen, ob eine gesellschaftliche Selbstregulierung einer staatlichen Regulierung vorzuziehen ist.</li></ol>`,
                linkedBibEntries: ['GGO'],
                linkedModules: ['zeit', 'rv', 'evor'],
              },
              zeitplanung112: {
                nameLink: '1.1.2 Zeitplanung',
                content: `<p>Die <a href="/#/zeit">Zeitplanung</a> ist ein wichtiger Arbeitsschritt zu Beginn eines Regelungsvorhabens. Es empfiehlt sich, bei der Zeitplanung vom geplanten Zeitpunkt des Inkrafttretens eines Gesetzes auszugehen und von diesem Zeitpunkt rückwirkend die Zeitpunkte der einzelnen Arbeitsschritte zu bestimmen. Der Zeitplan sollte stets aktualisiert werden. Eine Hilfestellung hierzu bietet die Anwendung&nbsp;<a href="/#/zeit">Elektronische Zeitplanung</a>.</p><h3>Kurzübersicht über die Schritte des Gesetzgebungsverfahrens und ihre Dauer</h3><p>Die angegebenen Zeiträume sind jeweils Schätz- bzw. Erfahrungswerte. Es empfiehlt sich, zeitliche Puffer einzuplanen.</p><p><strong><a href="#/evir/gesetz/regierungsinitiative/vorueberlegungen11">Vorphase</a></strong></p><ul><li>Konzeptionelle Überlegungen: Anlass des Tätigwerdens (z.B. Weisung der Hausleitung, Umsetzung des Koalitionsvertrags, ressortabgestimmtes Eckpunkte-Papier, EU-Richtlinie), Prüfung der politischen, inhaltlichen und zeitlichen Rahmenbedingungen, Prüfung von Regelungsalternativen und Gesetzesfolgen inkl. Erfüllungsaufwand</li><li>Ergänzung der Vorhabenplanung der BReg bis spätestens montags vier Wochen vor geplanter Kabinettbefassung (Unterrichtung des BK-Amtes gemäß <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 40 GGO</a>); ggf. PKP-Erfassung</li><li>Erstellung des Referentenentwurfs (Vorblatt, Gesetzentwurf, Begründung) unter Beachtung insbesondere der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 42, 43 und 44 GGO</a> und ggf. der einschlägigen Hausanordnungen</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/hausabstimmung13/verfahrenaufarbeitsundstaatssekretaersebene131">Hausabstimmung</a></strong> (Dauer: ca. zwei bis vier Wochen)</p><ul><li>Beachtung der ggf. einschlägigen Hausanordnungen</li><li>Beteiligung organisatorisch und fachlich betroffener Referate</li><li>ggf. Erörterung und Umsetzung der Änderungswünsche</li><li>frühzeitige Kontaktaufnahme mit dem Kabinett- und Parlamentreferat insbesondere mit Blick auf den Zeitplan für die Kabinettbefassung und das weitere Verfahren</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/hausabstimmung13/entscheidunghausleitung132">Befassung Hausleitung</a></strong> (mind. eine Woche vor Ressortabstimmung)</p><ul><li>Hausentwurf über festgelegten Dienstweg (z.B. Einbeziehung Kabinettreferat) an Hausleitung zur Billigung</li><li>nach Billigung durch die Hausleitung ggf. Vorabunterrichtung der Koalitionsfraktionen (Gelegenheit zur Stellungnahme)</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/ressortabstimmungundweiterebeteiligungen14">Ressortabstimmung und weitere Beteiligungen</a></strong> (Dauer: ca. vier Wochen (kürzere Frist möglich, insb. i.V.m. Ressortabstimmungen in Vorphase))</p><ul><li>rechtzeitige Beteiligung der betroffenen Ressorts, des Nationalen Normenkontrollrates, der Verfassungsressorts BMI und BMJV (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 45, 46 GGO, Anlage 6 zu § 45 Absatz 1, § 74 Absatz 5 GG</a>O), der Beauftragten, ggf. unter Fristsetzung (für Schlussabstimmung grundsätzlich mindestens vier Wochen, bei umfangreichen und schwierigen Entwürfen auf Antrag acht Wochen, nur in begründeten Ausnahmefällen kürzer (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 50 GGO</a>))</li><li>Beachtung der Vorhabenplanung der Bundesregierung; ggf. Nachholen der PKP-Erfassung</li></ul><p>Ggf. während Ressortabstimmung, ggf. nach Billigung durch Ressorts:</p><ul><li>Beteiligung der Länder, der kommunalen Spitzenverbände, der Fachkreise (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 47 GGO</a>)</li><li>bei Länder- und Verbändebeteiligung zeitgleiche Kenntnisgabe des Entwurfs an die Geschäftsstellen der Fraktionen des Deutschen Bundestages und des Bundesrates (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 48 Absatz 2 GGO</a>), danach Veröffentlichung im Internet</li><li>ggf. Erörterung und Umsetzung der Änderungswünsche</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/kabinettbeschluss16/kabinettvorlageanhausleitung162">Kabinettvorlage an Hausleitung</a></strong> (spätestens freitags zehn Tage vor gepl. Kabinetteinbringung (Begründete Nachmeldung mit St- Vorlage bis freitags 12 Uhr vor Kabinett mgl.))</p><p>Beachtung der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 22, 23, 51 GGO</a> und ggf. der einschlägigen Hausanordnung</p><p>Vorlage folgender Dokumente auf Dienstweg (z.B. Kabinettreferat) an Hausleitung:</p><ul><li>Kabinettvorlage (Außenverteiler [Anzahl nach <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 23 Absatz 1 GGO</a> vom BK festgelegt] plus ressortinterner Verteiler) bestehend aus (ggf. Hausanordnung für einschlägiges Muster beachten):</li><li>Ministeranschreiben an ChefBK</li><li>Anlage 1: Beschlussvorschlag</li><li>Anlage 2: Sprechzettel für den Regierungssprecher</li><li>Anlage 3: Vorblatt, Gesetzentwurf, Begründung zum Gesetzentwurf</li><li>Anlage 4: Stellungnahme des Nationalen Normenkontrollrates</li><li>Zeitplan</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/kabinettbeschluss16/kabinettbeschluss164">Kabinettbeschluss</a></strong> (mittwochs oder in Ausnahmen Umlaufverfahren)</p><p><strong><a href="#/evir/gesetz/regierungsinitiative/stellungnahmebundesrat17">Zuleitung an Bundesrat durch BK-Amt</a></strong> (sechs Wo. vor nächster BR- Sitzung (Fristverkürzung möglich))</p><ul><li>Beachtung des Sitzungskalenders<br>Stellungnahme des Bundesrates innerhalb von sechs Wochen (Artikel 76 Absatz 2 Satz 2 GG); Fristverkürzung zur Stellungnahme durch begründeten Antrag der Bundesregierung gegenüber dem Ständigen Beirat des Bundesrates möglich</li><li>ggf. Entwurf der Gegenäußerung der BReg zur Stellungnahme des Bundesrates und Herbeiführung des Kabinettbeschlusses hierüber (beachte <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 53 GGO</a>).<br>nach Befassung im Bundesrat (Ausnahme: Eilbedürftigkeit)</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/bundestag19">Zuleitung an Bundestag</a></strong> (nach Befassung im Bundesrat (Ausnahme: Eilbedürftigkeit))</p><ul><li>Gesetzentwurf, Stellungnahme des Bundesrates, Gegenäußerung BReg. Bei Eilbedürftigkeit (Kabinettbeschluss hierüber erforderlich): Weiterleitung drei Wochen nach Zuleitung an den Bundesrat möglich. Die Stellungnahme des Bundesrates muss dann umgehend nachgereicht werden (Artikel 76 Absatz 2 Satz 4 GG).</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/beratungbundestag21">Befassung Bundestag</a></strong> (Dauer: in der Regel mind. drei Wochen)</p><p>Beachtung des Sitzungskalenders</p><ul><li>Erste Lesung (Donnerstag oder Freitag), Überweisung an Ausschuss, Befassung im Ausschuss (Mittwoch), ggf. Anhörung</li><li>ggf. Entwurf von Formulierungsvorschlägen bei Änderungswünschen<br>Zweite und dritte Lesung (Do. oder Fr.), anschl. Weiterleitung an Bundesrat</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/beratungbundesrat22">Zweite Befassung Bundesrat</a></strong> (ca. zwei bis drei Wochen nach Zuleitung durch Bundestag)</p><p>Beachtung des Sitzungskalenders</p><ul><li>bei Einspruchsgesetz: BRat kann zustimmen oder innerhalb von drei Wochen Vermittlungsausschuss anrufen, danach innerhalb von zwei Wochen Einspruch einlegen, welchen der BT durch erneute Beschlussfassung zurückweisen kann</li><li>bei Zustimmungsgesetz: Beschlussfassung in angemessener Frist, Artikel 77 Absatz 2a GG; Zustimmung in der Regel in nächster BRat-Sitzung nach Zuleitung</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/ausfertigungundverkuendung31/herstellungderurschrift312">Herstellung der Urschrift</a></strong> (Dauer: ca. zwei Wochen)</p><ul><li>Beachtung der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 58, 59 GGO</a> und ggf. der einschlägigen Hausanweisungen</li><li>Veranlassung der Urschrift des Gesetzes bei der Schriftleitung des Bundesgesetzblattes (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 58 GGO</a>)</li><li>ggf. Bereinigung von Druckfehlern oder offensichtlichen Unrichtigkeiten im Berichtigungsverfahren (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 61 Absatz 2 GGO</a>)</li><li>Herbeiführung der Gegenzeichnung durch Mitglieder der BReg (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 58 Absatz 3 GGO</a>)</li></ul><p><strong><a href="#/evir/gesetz/regierungsinitiative/ausfertigungundverkuendung31/gegenzeichnungderurschrift313">Gegenzeichnung durch BK</a> und <a href="#/evir/gesetz/regierungsinitiative/ausfertigungundverkuendung31/ausfertigungdurchdenbundespraesidenten315">Ausfertigung durch BPräs</a></strong></p><p><strong><a href="#/evir/gesetz/regierungsinitiative/ausfertigungundverkuendung31/verkuendung316">Verkündung des Gesetzes</a> (Artikel 82 Absatz 1 GG; <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 60 GGO</a>)</strong></p><p><strong>Inkrafttreten des Gesetzes (Artikel 82 Absatz 2 GG)</strong> (14 Tage nach Verkündung, falls Inkrafttreten nicht im Gesetz selbst geregelt ist)</p>`,
                linkedBibEntries: [],
                linkedModules: ['zeit', 'rv'],
              },
              materialrecherche113: {
                nameLink: '1.1.3 Materialrecherche',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              eckpunkteundprospektivefolgenabschaetzung114: {
                nameLink: '1.1.4 Eckpunkte und prospektive Folgenabschätzung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              pruefungrahmenbedingungen115: {
                nameLink: '1.1.5 Prüfung: Rahmenbedingungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              unterrichtunghausleitung116: {
                nameLink: '1.1.6 Unterrichtung Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              unterrichtungbundeskanzleramt117: {
                nameLink: '1.1.7 Unterrichtung Bundeskanzleramt',
                content: `<p>Da der Bundeskanzler die Richtlinienkompetenz besitzt und das Regierungshandeln insgesamt koordiniert, ist das Bundeskanzleramt über das Vorhaben zu unterrichten. Dies geschieht gemäß § 40 GGO, sobald innerhalb des federführenden Ressorts entschieden ist, dass ein Gesetzentwurf ausgearbeitet werden soll. Dieser Zeitpunkt liegt damit in der Regel deutlich vor dem Beginn der Ressortabstimmung des Gesetzentwurfs. Falls die Arbeit am Vorhaben durch wichtige Vorgänge beeinflusst wird, ist dies dem Bundeskanzleramt ebenfalls mitzuteilen. Die Bundesregierung kann an diese Unterrichtung eine weiter gehende Steuerungsfunktion des Bundeskanzleramtes knüpfen (z.B. das Zustimmungserfordernis zwecks vorheriger Prüfung der „Koalitionsverträglichkeit"). Das kann im Einzelfall dazu führen, dass das Bundeskanzleramt ein Verfahren anhält. In diesen Fällen ist politisch zu klären, ob bzw. unter welchen Bedingungen das Verfahren wiederaufgenommen werden kann.</p><p>Die Unterrichtung des Bundeskanzleramtes erfolgt auf elektronischem Wege über <a href="/#/pkp">PKP</a>. Dafür meldet das federführende Ressort das Vorhaben in der übergreifenden Vorhabenplanung für die langfristige Vorhabenplanung an. Dies ersetzt das frühere Datenblattverfahren. Nähere Handlungsanweisungen für das Verfahren ergeben sich aus den hausinternen Handlungsanweisungen der einzelnen Ministerien. Ist – im Vorfeld eines Gesetzentwurfs – eine <a href="#/evir/gesetz/regierungsinitiative/vorueberlegungen11/eckpunkteundprospektivefolgenabschaetzung114">Festlegung von politischen Eckpunkten</a> vorgesehen, wird in der Regel das Bundeskanzleramt unterrichtet (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">siehe § 24 Absatz 1 GGO</a>); ggf. erfolgt eine Kabinettbefassung.</p>`,
                linkedBibEntries: ['GGO'],
                linkedModules: ['zeit', 'pkp'],
              },
            },
            arbeitsentwurf12: {
              nameLink: '1.2 Arbeitsentwurf',
              content: `<p class="ant-typography p-no-style">Dies ist ein Beispieltext. Dies ist ein optionaler Paragraf vor der ersten Überschrift. Neben ganz normalem Text kann man auch <strong>fett gedruckten</strong> oder <em>kursiven</em> Text anzeigen lassen. Eine minimale Seite könnte auch nur aus so einem Paragrafen bestehen ohne weitere Listen oder Überschriften.</p> <h2 class="ant-typography h-no-style">Das Level der ersten Überschrift muss immer 2 sein</h2> <h3 class="ant-typography h-no-style">geordnete Listen</h3> <p class="ant-typography p-no-style">Außerdem gibt es auch noch die Möglichkeit dem Nutzer weitere Informationen über Links zugänglich zu machen. Dabei unterscheiden wir vier verschiedene Arten von Links:</p> <ol> <li> <a href="#/evir/gesetz/regierungsinitiative/ressortabstimmung14">interner eViR-Link</a> </li> <li> <a href="#/regelungsvorhaben/archiv" target="_blank">interner Link</a> (nicht eViR) </li> <li><a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">externer Link&nbsp;</a> </li><li><a class="glossar-link">Link zu einem Glossareintrag</a></li> </ol> <h3 class="ant-typography h-no-style">ungeordnete Listen</h3> <p class="ant-typography p-no-style">Mehrere Paragrafen können direkt hintereinander kommen, ohne eine Überschrift dazwischen. </p> <p class="ant-typography p-no-style"> Falls man keine geordnete Liste verwenden möchte, kann man eine ungeordnete Liste anlegen: (Listen sollten generell nicht innerhalb von Paragrafen angelegt werden) </p> <ul> <li>Erster Punkt</li> <li>Zweiter Punkt</li> <li>Dritter Punkt</li> </ul> <h2 class="ant-typography h-no-style">Weiterer thematischer Block</h2> <p class="ant-typography p-no-style">Auf eine Überschrift kann entweder ein Paragraf oder eine Überschrift folgen. Allerdings dürfen zwei Überschriften des gleichen Levels nicht direkt hintereinander stehen (also ohne Paragraf dazwischen). </p> <h2 class="ant-typography h-no-style">Weitere Überschrift Level 2</h2> <h3 class="ant-typography h-no-style">Weitere Überschrift Level 3</h3> <h4 class="ant-typography h-no-style">Weitere Überschrift Level 4</h4> <p class="ant-typography p-no-style">Überschriften mit aufsteigendem Level können beliebig hintereinander gesetzt werden. </p>`,
              linkedBibEntries: ['GGO'],
              linkedModules: ['zeit', 'editor'],
              ersterentwurf121: {
                nameLink: '1.2.1 Erster Entwurf',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              gesetzesfolgenabschaetzung122: {
                nameLink: '1.2.2 Gesetzesfolgenabschätzung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              pruefungeinhaltungderanforderungen123: {
                nameLink: '1.2.3 Prüfung: Einhaltung der Anforderungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            hausabstimmung13: {
              nameLink: '1.3 Hausabstimmung',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              verfahrenaufarbeitsundstaatssekretaersebene131: {
                nameLink: '1.3.1 Verfahren auf Arbeits- und Staatssekretärsebene',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              entscheidunghausleitung132: {
                nameLink: '1.3.2 Entscheidung Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            ressortabstimmungundweiterebeteiligungen14: {
              nameLink: '1.4 Ressortabstimmung und weitere Beteiligungen',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              ressortabstimmung141: {
                nameLink: '1.4.1 Ressortabstimmung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beteiligungnkr142: {
                nameLink: '1.4.2 Beteiligung NKR',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              eroerterungderaenderungswuensche143: {
                nameLink: '1.4.3 Erörterung der Änderungswünsche',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              erneuteabstimmungimressort144: {
                nameLink: '1.4.4 Erneute Abstimmung im Ressort',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            laender15: {
              nameLink: '1.5 Länder- und Verbändebeteiligung',
            },
            kabinettbeschluss16: {
              nameLink: '1.6 Kabinettbeschluss',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              schlussabstimmung161: {
                nameLink: '1.6.1 Schlussabstimmung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettvorlageanhausleitung162: {
                nameLink: '1.6.2 Kabinettvorlage an Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettvorlageanbundeskanzleramt163: {
                nameLink: '1.6.3 Kabinettvorlage an Bundeskanzleramt',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettbeschluss164: {
                nameLink: '1.6.4 Kabinettbeschluss',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            stellungnahmebundesrat17: {
              nameLink: '1.7 Stellungnahme Bundesrat',
              content: `<p>Der Bundesrat ist zur Stellungnahme zu einem Gesetzentwurf der Bundesregierung berechtigt, jedoch nicht verpflichtet (Artikel 76 Absatz 2 Satz 2 GG). Die Stellungnahme des Bundesrates ist für keinen Verfahrensbeteiligten bindend. Er kann daher bei der späteren Beratung des Gesetzesbeschlusses nach Artikel 77 Absatz 2 GG (sog. zweiter Durchgang im Bundesrat) ohne Weiteres anders votieren. Der Regierungsentwurf erfährt im Bundestag vielfach Änderungen, mit denen sich der Bundesrat daher erst im zweiten Durchgang befassen kann.</p><p>Die reguläre Frist, die dem Bundesrat für seine Stellungnahme gesetzt ist, beträgt sechs Wochen (Artikel 76 Absatz 2 Satz 2 GG). Von dieser Regel gibt es folgende Ausnahmen:</p><p><ul><li>Die Frist beträgt neun Wochen ohne besonderes Verlangen des Bundesrates, wenn der Regierungsentwurf Änderungen des GG oder die Übertragung von Hoheitsrechten nach den Artikeln 23, 24 GG vorsieht (Artikel 76 Absatz 2 Satz 5 GG).</li><li>Auf ausdrückliches Verlangen des Bundesrates, insbesondere wenn die Regierungsvorlage sehr umfangreich ist, beträgt die Frist ebenfalls neun Wochen (Artikel 76 Absatz 2 Satz 3 GG).</li><li>Hat die Bundesregierung eine Vorlage bei der Zuleitung an den Bundesrat als <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/35">besonders eilbedürftig</a> bezeichnet, kann sie dem Bundestag die Vorlage bereits nach drei Wochen zuleiten, auch wenn die Stellungnahme des Bundesrates noch nicht bei ihr eingegangen ist (Artikel 76 Absatz 2 Satz 4, 1. Alternative GG). In diesem Fall hat die Bundesregierung dem Bundestag die Stellungnahme des Bundesrates unverzüglich nach Eingang nach­ zureichen.</li></ul></p><p>In den Fällen der Fristverlängerung von sechs auf neun Wochen kann eine frühere Zuleitung an den Bundestag jedoch nicht bereits nach drei, sondern erst nach sechs Wochen erfolgen (Artikel 76 Absatz 2 Satz 4, 2. Alternative GG). Eine Fristverkürzung wegen Eilbedürftigkeit ist ausgeschlossen, wenn der Regierungsentwurf Änderungen des Grundgesetzes oder die Übertragung von Hoheitsrechten nach den Artikeln 23, 24 GG vorsieht (Artikel 76 Absatz 2 Satz 5, letzter Halbsatz GG).</p><p><strong>Sonderregelung für Haushaltsvorlagen</strong></p><p>Zu beachten ist die Sonderregelung des Artikels 110 Absatz 3 GG, die für Haushaltsvorlagen gilt. Diese werden gleichzeitig mit der Zuleitung an den Bundesrat beim Bundestag eingebracht. Der Bundesrat ist in diesem Fall berechtigt, innerhalb von sechs Wochen, bei Änderungsvorlagen zum Haushaltsgesetz innerhalb von drei Wochen, Stellung zum Regierungsentwurf zu nehmen (Artikel 110 Absatz 3 GG).</p>`,
              linkedBibEntries: ['Fristverkürzte Zuleitung'],
              linkedModules: ['zeit'],
              ausschussberatungen171: {
                nameLink: '1.7.1 Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              einleitungausschussberatungen172: {
                nameLink: '1.7.2 Einleitung Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              ablaufausschussberatungen173: {
                nameLink: '1.7.3 Ablauf Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              abstimmungenindenausschuessen174: {
                nameLink: '1.7.4 Abstimmungen in den Ausschüssen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              abschlussausschussberatungen175: {
                nameLink: '1.7.5 Abschluss Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beschlussdesplenums176: {
                nameLink: '1.7.6 Beschluss des Plenums',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              uebersendungderstellungnahmeanbundesregierung177: {
                nameLink: '1.7.7 Übersendung der Stellungnahme an Bundesregierung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            gegenaeusserungbundesregierung18: {
              nameLink: '1.8 Gegenäußerung Bundesregierung',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              erstellunggegenaeusserung181: {
                nameLink: '1.8.1 Erstellung Gegenäußerung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettentscheidunguebergegenaeusserung182: {
                nameLink: '1.8.2 Kabinettentscheidung über Gegenäußerung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              pflichtenundrechtevonbundestagbundesratundbundesregierung183: {
                nameLink: '1.8.3 Pflichten und Rechte von Bundestag, Bundesrat und Bundesregierung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            bundestag19: {
              nameLink: '1.9 Einbringung in den Bundestag',
            },
          },

          phaseTitle2: {
            nameLink: 'Phase 2 <br>Parlamentarisches Gesetzgebungsverfahren',
            beratungbundestag21: {
              nameLink: '2.1 Beratung Bundestag',
              content: `<p>Die Beratungen des Gesetzentwurfs im Parlament erfolgen in drei sog. Lesungen. Zwischen der ersten und der zweiten bzw. dritten Lesung wird der Entwurf in den Parlamentsausschüssen beraten. Bevor der Gesetzentwurf überhaupt behandelt werden kann, muss er auf die Tagesordnung des Plenums gesetzt werden. Darüber, ob und wenn ja, wann ein Gesetzentwurf auf die Tagesordnung kommt, sowie darüber, ob eine Aussprache erfolgt und wenn ja, wie lange sie dauern soll, befindet im Allgemeinen der Ältestenrat (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go06/245164" target="_blank">§ 20 Absatz 1 GO-BT</a>).</p><h3>Weitere Akteure im Gesetzgebungsverfahren</h3><p>Maßgebliche Akteure in den Ausschussberatungen sind die Ausschussvorsitzenden, die Ausschussmitglieder, die sog.&nbsp; Obleute der Fraktionen, die Berichterstatter und die fachpolitischen Sprecher der Fraktionen. Die Obleute sind Ansprechpartner ihrer Fraktionen in allen Geschäftsführungsfragen der Ausschussarbeit; sie sind nicht zu verwechseln mit den Berichterstattern, deren Aufgabe darin besteht, für den jeweiligen Gegenstand der Ausschussarbeit (Gesetzentwürfe oder Anträge) die fachliche und politische Federführung im Ausschuss zu übernehmen. Von den Obleuten und Berichterstattern wiederum zu unterscheiden sind die jeweiligen fachpolitischen Sprecher der Fraktionen, deren Aufgabe darin besteht, das gesamte Feld der jeweiligen Fachpolitik politisch zu definieren, die Einzelbereiche zu koordinieren und das Politikfeld innerhalb der Fraktion und in der Öffentlichkeit zu vertreten. Die Funktionen sind zwar zu trennen, gleichwohl können sie von einer Person ausgeübt werden. So kann ein fachpolitischer Sprecher zugleich Obmann und Berichterstatter für einen Gesetzentwurf sein. Die Regel ist aber, dass fachpolitischer Sprecher und Obmann nicht identisch sind.</p>`,
              linkedBibEntries: [],
              linkedModules: ['zeit'],
              erstelesung211: {
                nameLink: '2.1.1 Erste Lesung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              ausschussberatungen212: {
                nameLink: '2.1.2 Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              rollederbundesregierunginausschussberatungen213: {
                nameLink: '2.1.3 Rolle der Bundesregierung in Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              berichterstattergespraeche214: {
                nameLink: '2.1.4 Berichterstattergespräche',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beschlussempfehlungundberichtdesfederfuehrendenausschusses215: {
                nameLink: '2.1.5 Beschlussempfehlung und Bericht des federführenden Ausschusses',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              zweitelesung216: {
                nameLink: '2.1.6 Zweite Lesung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              drittelesung217: {
                nameLink: '2.1.7 Dritte Lesung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              schlussabstimmung218: {
                nameLink: '2.1.8 Schlussabstimmung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              einbringungdesgesetzesbeschlussesindenbundesrat219: {
                nameLink: '2.1.9 Einbringung des Gesetzesbeschlusses in den Bundesrat',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            beratungbundesrat22: {
              nameLink: '2.2 Beratung Bundesrat',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              ausschussberatungen221: {
                nameLink: '2.2.1 Ausschussberatungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beschlussdesplenums222: {
                nameLink: '2.2.2 Beschluss des Plenums',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              anrufungdesvermittlungsausschusses223: {
                nameLink: '2.2.3 Anrufung des Vermittlungsausschusses',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            beratungvermittlungsausschuss23: {
              nameLink: '2.3 Beratung Vermittlungsausschuss',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              vermittlungsverfahren231: {
                nameLink: '2.3.1 Vermittlungsverfahren',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              vermittlungsergebnisse232: {
                nameLink: '2.3.2 Vermittlungsergebnisse',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beschlussfassungvonbundestagundbundesrat233: {
                nameLink: '2.3.3 Beschlussfassung von Bundestag und Bundesrat',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
          },

          phaseTitle3: {
            nameLink: 'Phase 3 <br>Abschlussverfahren',
            ausfertigungundverkuendung31: {
              nameLink: '3.1 Ausfertigung und Verkündung',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              veranlassungderurschrift311: {
                nameLink: '3.1.1 Veranlassung der Urschrift',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              herstellungderurschrift312: {
                nameLink: '3.1.2 Herstellung der Urschrift',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              gegenzeichnungderurschrift313: {
                nameLink: '3.1.3 Gegenzeichnung der Urschrift',
                content: `<p>Das federführende Ressort veranlasst, dass die Urschrift (Artikel 82 Absatz 1 Satz 1 GG in Verbindung mit Artikel 58 Satz 1 GG) von den zuständigen Mitgliedern der Bundesregierung gegengezeichnet wird (die Zeichnungsregelungen ergeben sich aus <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 58 Absatz 3 Satz 1, 2 sowie aus Absatz 5 und 6 GGO</a> in Verbindung mit&nbsp;<a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">§ 14 GOBReg</a>). Die Gegenzeichnung der Bundesregierung erfolgt durch den federführenden Minister, den Bundeskanzler und die Minister der Ressorts, deren Belange wesentlich betroffen sind. Gesetze, die die von der Bundesregierung vorgeschlagenen Ausgaben des Haushaltsplanes erhöhen oder die neuen Ausgaben erfordern (Artikel 113 GG), sind vom Bundesminister der Finanzen gegenzuzeichnen.</p>`,
                linkedBibEntries: ['GGO', 'GOBReg'],
                linkedModules: ['zeit'],
              },
              uebersendungderurschriftanbundespraesidialamt314: {
                nameLink: '3.1.4 Übersendung der Urschrift an Bundespräsidialamt',
                content: `<p>Nach erfolgter Gegenzeichnung durch den federführenden Minister und die Minister der Ressorts, deren Belange wesentlich betroffen sind, hat das federführende Fachreferat die Urschrift, soweit sie aus mehreren Blättern besteht, mit einer schwarz-rot-goldenen Schnur sowie mit dem großen Bundessiegel siegeln zu lassen. Die Formvorgaben des <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 59 Absatz 1 GGO</a> sind zu beachten. Anschließend erfolgt die Zuleitung an das Bundeskanzleramt. Das Bundeskanzleramt leitet die Urschrift nach Gegenzeichnung durch den Bundeskanzler an das Bundespräsidialamt weiter.</p>`,
                linkedBibEntries: ['GGO'],
                linkedModules: ['zeit'],
              },
              ausfertigungdurchdenbundespraesidenten315: {
                nameLink: '3.1.5 Ausfertigung durch den Bundespräsidenten',
                content: `<p>Der Bundespräsident prüft das verfassungskonforme Zustandekommen des Gesetzes und ob sonstige, offenkundige Verstöße gegen Bestimmungen des Grundgesetzes gegeben sind. Bejaht er – was die Regel ist – die Verfassungskonformität des Gesetzes, unterzeichnet er die Urschrift. Außerdem wird das Datum der Unterzeichnung durch den Bundespräsidenten eingesetzt. Sodann übermittelt das Bundespräsidialamt die Urschrift der Schriftleitung des BGBI. zur Verkündung und unterrichtet die beteiligten Ressorts über die Ausfertigung des Gesetzes (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 60 Satz 1, 2 GGO</a>).</p><p><strong>Fall der Verweigerung der Unterzeichnung durch den Bundespräsidenten<br></strong></p><p>Der Bundespräsident hat das Recht, ihm zur Ausfertigung vorgelegte Gesetze daraufhin zu prüfen, ob sie verfassungskonform sind. Verstößt ein Gesetz nach seiner Einschätzung gegen Verfahrensbestimmungen des GG oder ist es nach seiner Auffassung materiell verfassungswidrig, so kann der Bundespräsident die Ausfertigung des Gesetzes verweigern. Das Gesetz tritt damit nicht in Kraft. In einem solchen Fall bleibt dem Deutschen Bundestag und dem Bundesrat die Möglichkeit, die Verfassungskonformität der Norm und damit die Rechtswidrigkeit der Weigerung des Bundespräsidenten durch das Bundesverfassungsgericht im Rahmen eines sog. Organstreitverfahrens (Artikel 93 Absatz 1 Nummer 1 GG) prüfen zu lassen.<br></p>`,
                linkedBibEntries: ['GGO'],
                linkedModules: ['zeit'],
              },
              verkuendung316: {
                nameLink: '3.1.6 Verkündung',
                content: `<p>Die Schriftleitung des BGBI. veranlasst die Verkündung (d.h. die amtliche Bekanntgabe) des Gesetzeswortlauts im Bundesgesetzblatt. Damit existiert das Gesetz zwar rechtlich, tritt aber – wenn es selbst keine anderweitige Inkrafttretensregelung enthält – erst 14 Tage nach Ablauf des Tages in Kraft, an dem es im BGBI. verkündet wurde.</p><p>Die Schriftleitung des BGBI. unterrichtet das Bundeskanzleramt und das federführende Ressort über die Verkündung und gibt die Urschrift des Gesetzes zur Archivierung an das Bundesarchiv ab.</p><p>Das Gesetzgebungsverfahren ist damit abgeschlossen.</p>`,
                linkedBibEntries: [],
                linkedModules: ['zeit'],
              },
            },
          },
        },
        bundestagsinitiative: {
          nameLink: 'Bundestagsinitiative',
        },
        bundesratsinitiative: {
          nameLink: 'Bundesratsinitiative',
        },
      },
      rechtsverordnung: {
        nameLink: 'Rechtsverordnung',
        rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister: {
          nameLink: 'Rechtsverordnung der Bundesregierung oder eines oder mehrerer Bundesminister',
          content: `<p>Der Erlass von Gesetzen ist grundsätzlich den gesetzgebenden Körperschaften vorbehalten. Das Grundgesetz formuliert jedoch in Artikel 80 GG eine Ausnahme. Der Exekutive wird in den dort formulierten Grenzen eine Rechtsetzungsbefugnis zuerkannt.</p>
                          <p>Es gibt verschiedene Anlässe für den Erlass einer Rechtsverordnung, so z. B. einen gesetzlichen, politischen oder richterlichen Auftrag.</p>
                          <p>Eine <a href="https://www.verwaltung-innovativ.de/DE/Gesetzgebung/Projekt_eGesetzgebung/Handbuecher_Arbeitshilfen_Leitfaeden/Hb_vorbereitung_rechts_u_verwaltungsvorschriften/Teil_I_%20Rahmenbedingungen/1.1_Normtypen_und_Normenhierarchie/1.1_normtypen_node.html#doc7625304bodyText3" target="_blank">Rechtsverordnung</a> ist kein Gesetz im formellen Sinne, auf das die Gesetzgebungsvorschriften der Artikel 70 ff. GG anwendbar wären. Eine Rechtsverordnung stellt eine untergesetzliche Form des Rechts dar, weshalb hier nicht die Rede von Gesetzgebung sein kann. Man spricht daher beim Erlass von Rechtsverordnungen von Verordnungsgebung.</p>
                          <p>Gleichwohl stellen Rechtsverordnungen Gesetze im materiellen Sinne dar. Sie müssen sich auf ein ordnungsgemäß zustande gekommenes Parlamentsgesetz stützen. Im Rahmen der durch das Parlamentsgesetz getroffenen Ermächtigung wird die Normsetzungsbefugnis partiell auf die Exekutive als sogenanntem Verordnungsgeber oder Delegatar übertragen.</p>
                          <p>Dabei gilt im Kern der Grundsatz: Das Parlamentsgesetz schafft den Rahmen und ermächtigt den Verordnungsgeber explizit zur Normsetzung der „technischen" Details. Die Einzelregelungen der Rechtsverordnung wiederum begründen auch Rechte und Pflichten.</p>`,
          phaseTitle1: {
            nameLink: 'Phase 1 <br>Erstellung und Beschluss einer Rechtsverordnung',
            vorueberlegungen11: {
              nameLink: '1.1 Vorüberlegungen',
              content: `<p>Es ist zu empfehlen, eine ähnliche Vorgehensweise für die Erstellung eines Entwurfes für eine Rechtsverordnung zu wählen wie für die Erstellung eines Gesetzes im formellen Sinn. Für Entwürfe von Rechtsverordnungen gelten die Bestimmungen der GGO über die Vorbereitung und Fassung der Gesetzentwürfe entsprechend (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 62 Absatz 2 GGO</a>).</p><p>Die Federführung für die Erarbeitung eines Verordnungsentwurfs liegt bei dem Ressort, das zum Erlass einer Rechtsverordnung ermächtigt ist bzw. dem das Sachgebiet nach der Geschäftsverteilung der Bundesregierung zugewiesen ist. Die Geschäftsverteilung erfolgt durch Organisationserlass des Bundeskanzlers.</p><p>Das federführende Ressort – und dort die zuständige Organisationseinheit, die mit der Durchführung des Rechtsetzungsverfahrens beauftragt ist – trägt die Gesamtverantwortung für die fachlichen Belange und die Einhaltung eines ordnungsgemäßen Verfahrens.</p><p>Im Regelfall findet im Prozess der Erarbeitung des Verordnungsentwurfs eine Rückkopplung zwischen der zuständigen Organisationseinheit und der Leitung des Ressorts statt. So kann beispielsweise eine Leitungsvorlage zur Notwendigkeit einer Neuregelung und zu deren Zielen zu Eckpunkten des Vorhabens oder zu beabsichtigten Konsultationen notwendig sein. Eine Ausnahme von diesem Regelfall kann z. B. eine alternativlose Umsetzung von europarechtlichen Vorgaben in nationales Recht sein.</p><p>Eine Abstimmung des Verordnungsentwurfs mit anderen Ministerien muss erfolgen, falls deren Belange betroffen sind.</p><p><strong>Anlässe für den Erlass einer Rechtsverordnung können sich insbesondere ergeben durch:<br></strong></p><ul><li>gesetzliche Verpflichtung</li><li>gerichtliche Entscheidung</li><li>(Teil-)Nichtigkeit einer bestehenden Rechtsverordnung</li><li>Weisung der Hausleitung</li><li>Umsetzung des Koalitionsvertrags</li><li>ressort-abgestimmtes Eckpunkte-Papier</li><li>Gerichtsentscheidung</li><li>Bevorstehendes Auslaufen vorhandener Regelungen (befristete Rechtsverordnungen)</li><li>Evaluierung des geltenden Rechts</li><li><a href="/egesetzgebung-platform-backend/arbeitshilfen/download/4">EU-Recht</a></li><li><a href="/egesetzgebung-platform-backend/arbeitshilfen/download/3">Abschluss völkerrechtlicher Verträge</a> (Vertragsgesetz und ggf. erforderliche Umsetzung in nationales Recht)</li></ul>`,
              linkedBibEntries: [
                'AH Experimentierklauseln',
                'EU-Handbuch',
                'GGO',
                'Leitfaden vV',
                'RiVeVo',
                'RvV',
                'Zusammenarbeit EU',
              ],
              linkedModules: ['zeit', 'rv'],
              zeitplanung111: {
                nameLink: '1.1.1 Zeitplanung',
                content: `<p>Die Zeitplanung ist ein wichtiger Arbeitsschritt zu Beginn eines jeden Regelungsvorhabens. Es empfiehlt sich auch bei der Erarbeitung einer Rechtsverordnung, eine Zeitplanung zu erstellen, dabei vom geplanten Zeitpunkt des Inkrafttretens der Rechtsverordnung auszugehen und von diesem Zeitpunkt aus rückwirkend die Zeitpunkte der einzelnen Arbeitsschritte zu bestimmen.</p><h3>Gemeinsame Erarbeitung von Stammgesetz mit Verordnungsermächtigung und Rechtsverordnung</h3><p>Nicht selten werden in einem Artikelgesetz sowohl das Stammgesetz mit der Verordnungsermächtigung als auch die Rechtsverordnung selbst zusammen erarbeitet. Dies hat mitunter Vorteile, da sich im Laufe der Erarbeitung der Rechtsverordnung ergeben kann, dass der Entwurf der Verordnungsermächtigung im Stammgesetz zu weit oder zu eng formuliert wurde und so noch im laufenden Verfahren der Erarbeitung des Gesetzentwurfes Anpassungsmöglichkeiten bestehen und genutzt werden können, was bei einer zeitlich getrennten Erarbeitung nicht möglich ist, ohne auch das Stammgesetz wieder im förmlichen Verfahren abzuändern. Dann allerdings muss das entwurfsverfassende Bundesministerium darauf achten, dass für das Stammgesetz einerseits und die Rechtsverordnung andererseits unterschiedliche Inkrafttretenstermine festgelegt werden, um die Anforderung aus <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 66 Absatz 1 GGO</a> zu erfüllen. Danach ist eine Rechtsverordnung erst auszufertigen, nachdem die ermächtigende Gesetzesbestimmung in Kraft getreten ist. Dieser Umstand muss bereits bei der Zeitplanung berücksichtigt werden, um entsprechende Gestaltungsmöglichkeiten zu wahren.</p><h4>Kurzübersicht über die Schritte des Verfahrens bei der Erstellung einer Rechtsverordnung und ihre Dauer</h4><p>Die angegebenen Zeiträume sind jeweils Schätz- bzw. Erfahrungswerte. Es empfiehlt sich, zeitliche Puffer einzuplanen.</p><p><strong><a href="#/evir/rechtsverordnung/vorueberlegungen51">Vorphase</a></strong></p><ul><li>konzeptionelle Überlegungen: Anlass des Tätigwerdens (z. B. gesetzliche Verpflichtung, gerichtliche Entscheidung, (Teil-)Nichtigkeit einer bestehenden Rechtsverordnung, Weisung der Hausleitung, Umsetzung des Koalitionsvertrags, ressortabgestimmtes Eckpunkte-Papier), Prüfung der politischen, inhaltlichen und zeitlichen Rahmenbedingungen, Prüfung von Regelungsalternativen und Regelungsfolgen inkl. Erfüllungsaufwand</li><li>Erstellung des Referentenentwurfs (Vorblatt, Verordnungsentwurf, Begründung) unter Beachtung insbesondere der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 42, 43, 44 und 62 GGO</a> und ggf. der einschlägigen Hausanordnungen</li></ul><strong><a href="#/evir/rechtsverordnung/hausabstimmung53/verfahrenaufarbeitsundstaatssekretaersebene531">Hausabstimmung</a></strong> (Dauer: ca. zwei bis vier Wochen)<br><ul><li>Beachtung ggf. der einschlägigen Hausanordnungen</li><li>Beteiligung organisatorisch und fachlich betroffener Referate</li><li>ggf. Erörterung und Umsetzung der Änderungswünsche</li><li>frühzeitige Kontaktaufnahme mit dem Kabinett- und Parlamentreferat insbesondere mit Blick auf den Zeitplan für die Kabinettbefassung und das weitere Verfahren</li></ul><p><strong>Befassung Hausleitung</strong> (mind. eine Woche vor Ressortabstimmung)</p><ul><li>Hausentwurf über festgelegten Dienstweg (z. B. Einbeziehung Kabinettreferat) an Hausleitung zur Billigung</li><li>nach Billigung durch die Hausleitung ggf. Vorabunterrichtung der Koalitionsfraktionen (Gelegenheit zur Stellungnahme)</li></ul><p><strong>Ressortabstimmung und weitere Beteiligungen</strong> (Dauer: ca. vier Wochen (kürzere Frist möglich, insb. i.V.m. Ressortabstimmungen in Vorphase))</p><ul><li>rechtzeitige Beteiligung betroffener Ressorts, des Nationalen Normenkontrollrates, der Verfassungsressorts BMI und BMJV (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 45, 46 GGO, Anlage 6 zu § 45 Absatz 1, § 74 Absatz 5 GGO</a>), der Beauftragten, ggf. unter Fristsetzung (für Schlussabstimmung grundsätzlich mindestens vier Wochen, bei umfangreichen und schwierigen Entwürfen auf Antrag acht Wochen, nur in begründeten Ausnahmefällen kürzer - § 50 GGO)</li><li>Beachtung der Vorhabenplanung der Bundesregierung; ggf. Nachholen der PKP-Erfassung</li><li>Ggf. während Ressortabstimmung, ggf. nach Billigung durch Ressorts:</li><li>Beteiligung der Länder, kommunaler Spitzenverbände, Fachkreise (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 47 GGO</a>)</li><li>bei Länder- und Verbändebeteiligung zeitgleiche Kenntnisgabe des Entwurfs an die Geschäftsstellen der Fraktionen des Deutschen Bundestages und an den Bundesrat (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 48 Absatz 2 GGO</a>), danach Veröffentlichung im Internet</li><li>ggf. Erörterung und Umsetzung der Änderungswünsche</li></ul><p><strong>Kabinettvorlage an Hausleitung</strong> (spätestens freitags zehn Tage vor gepl. Kabinetteinbringung (Begründete Nachmeldung mit St-Vorlage bis freitags 12 Uhr vor Kabinett mgl.))</p><p>Beachtung der §§ 22, 23, 51 GGO und ggf. der einschlägigen Hausanordnung</p><p>Vorlage auf Dienstweg (z. B. Kabinettreferat) an Hausleitung</p><ul><li>Kabinettvorlage (Außenverteiler [Anzahl nach <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 23 Absatz 1 GGO</a> vom BK festgelegt] plus ressortinterner Verteiler) bestehend aus (Beachtung ggf. der Hausanordnung für einschlägiges Muster):</li><li>Ministeranschreiben an ChefBK</li><li>Anlage 1: Beschlussvorschlag</li><li>Anlage 2: Sprechzettel für den Regierungssprecher</li><li>Anlage 3: Vorblatt, Verordnungsentwurf, Begründung zum Verordnungsentwurf</li><li>Anlage 4: Stellungnahme des Nationalen Normenkontrollrates</li><li>Zeitplan</li></ul><p><strong>Kabinettbeschluss </strong>(mittwochs oder in Ausnahmen Umlaufverfahren)</p><p><strong>Zuleitung an Bundestag durch BK-Amt oder ChefBK</strong> (nach Beschluss der Bundesregierung )</p><ul><li>Rechtsverordnungen der Bundesregierung, die der Zustimmung des Bundestages bedürfen, werden gem. § 92 GO BT durch den Präsidenten des Bundestages unter Bestimmung einer Beratungsfrist an die zuständigen Ausschüsse überwiesen. Die Fristsetzung berücksichtigt die Beteiligungsfrist der Ermächtigungsnorm. Die Zuleitung durch die Bundesregierung soll daher so erfolgen, dass die Überweisung spätestens in der auf die Zuleitung folgenden Woche erfolgen kann; die Folgewoche muss also eine Sitzungswoche sein. Abweichende Zuleitungen sollen mit dem Parlamentssekretariat abgestimmt werden.</li></ul><p><strong>Bei Maßgabebeschlüssen des Bundestages</strong></p><ul><li>Beachtung von <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 62 GGO, § 65</a> gilt analog</li><li>Rechtsverordnungen der BReg und Rechtsverordnungen, die dem Kabinett vorzulegen sind (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 62 Absatz 3 GGO</a>): müssen in der durch den Maßgabebeschluss geänderten Fassung erneut von der BReg beschlossen werden (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 65 Nummer 1 GGO</a>); Kabinettvorlage des federführenden Bundesministeriums erforderlich; bei Änderungen gegenüber dem Maßgabebeschluss ist eine erneute Vorlage an den Bundestag notwendig</li><li>Rechtsverordnungen eines oder mehrerer Bundesministerien, die nicht dem Kabinett vorzulegen sind (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 65 Nr. 3 GGO</a>): müssen in der durch den Maßgabebeschluss geänderten Fassung erneut von dem oder den Bundesministern gebilligt werden (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 64 Absatz 2 GGO</a>); bei Änderungen gegenüber dem Maßgabebeschluss ist eine erneute Vorlage über ChefBK an den Bundestag notwendig</li></ul><p><strong>Zuleitung an Bundesrat durch BK-Amt oder ChefBK</strong> (nach Beschluss der Bundesregierung )</p><ul><li>Beachtung von <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 64 GGO</a></li><li>Beachtung des Sitzungskalenders: keine einzuhaltenden Fristen für den Zustimmungs-, Ablehnungs- oder Maßgabebeschluss des Bundesrates, jedoch Bemühung des Bundesrates um Einhaltung der gängigen 6-Wochen-Frist (dazu muss eine entsprechend fristgerechte Vorlage vor den Plenarsitzungen erfolgen)</li></ul><p><strong>Bei Maßgabebeschlüssen des Bundesrates</strong> (nach Befassung im Bundesrat (Dauer: i.d.R. zwischen 6 und 12 Wochen))</p><ul><li>Beachtung der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 62, 64, 65 GGO</a></li><li>Rechtsverordnungen der BReg und Rechtsverordnungen, die dem Kabinett vorzulegen sind (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 62 Absatz 3 GGO</a>): müssen in der durch den Maßgabebeschluss geänderten Fassung erneut von der BReg beschlossen werden (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 65 Nummer 1 GGO</a>); Kabinettvorlage des federführenden Bundesministeriums erforderlich; bei Änderungen gegenüber dem Maßgabebeschluss ist eine erneute Vorlage an den Bundesrat notwendig</li><li>Rechtsverordnungen eines oder mehrerer Bundesministerien, die nicht dem Kabinett vorzulegen sind (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 65 Nr. 3 GGO</a>): müssen in der durch den Maßgabebeschluss geänderten Fassung erneut von dem oder den Bundesministern gebilligt werden (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 64 Absatz 2 GGO</a>); bei Änderungen gegenüber dem Maßgabebeschluss ist eine erneute Vorlage über ChefBK an den Bundesrat notwendig</li><li>Sieht die Ermächtigungsnorm eine Beteiligung des Bundestages vor, ist die nach Maßgabe des Bundesrates geänderte Rechtsverordnung erneut dem Bundestag vorzulegen.</li></ul><p><strong>Ausfertigung und Vorbereitung der Verkündung der Rechtsverordnung</strong> (Dauer: in der Regel mind. drei Wochen)</p><ul><li>Beachtung von <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 66 GGO</a></li><li>Ausfertigung der Rechtsverordnung erst nach Inkrafttreten der Verordnungsermächtigung</li><li>nach Ausfertigung der Rechtsverordnung und nach Zustimmung des Bundestages und/oder Bundesrates (ggf. unter angenommenen Maßgaben) Übersendung an die Schriftleitung des Verkündungsorgans</li></ul><p><strong>Herstellung der Urschrift</strong> (Dauer ca. zwei Wochen)</p><ul><li>Beachtung von <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 67 GGO</a></li><li>Veranlassung der Herstellung der Urschrift der Rechtsverordnung nach Verabschiedung der endgültigen Fassung (und ggf. Zustimmung des Bundestages und/oder Bundesrates) bei der Schriftleitung des Verkündungsorgans durch das federführende Ministerium</li><li>ggf. Bereinigung von Druckfehlern oder offensichtlichen Unrichtigkeiten im Berichtigungsverfahren (<a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 61 Absatz 2 GGO</a>)</li></ul><p><strong>Verkündung der Rechtsverordnung</strong> (Dauer: ca. zwei Wochen)</p><ul><li>Beachtung der <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§§ 68, 76 GGO</a></li></ul><p><strong>Inkrafttreten der Rechtsverordnung</strong></p><ul><li>Beachtung von <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34">§ 66 Absatz 1 GGO</a></li></ul>`,
                linkedBibEntries: ['GGO'],
                linkedModules: ['zeit', 'rv'],
              },
              handlungsrahmenrechtsverordnung112: {
                nameLink: '1.1.2 Handlungsrahmen Rechtsverordnung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              materialrecherche113: {
                nameLink: '1.1.3 Materialrecherche',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              eckpunkteundprospektivefolgenabschaetzung114: {
                nameLink: '1.1.4 Eckpunkte und prospektive Folgenabschätzung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              pruefungrahmenbedingungen115: {
                nameLink: '1.1.5 Prüfung: Rahmenbedingungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              unterrichtunghausleitung116: {
                nameLink: '1.1.6 Unterrichtung Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              unterrichtungbundeskanzleramt117: {
                nameLink: '1.1.7 Unterrichtung Bundeskanzleramt',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            arbeitsentwurf12: {
              nameLink: '1.2 Arbeitsentwurf',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              ersterentwurf121: {
                nameLink: '1.2.1 Erster Entwurf',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              regelungsfolgenabschaetzung122: {
                nameLink: '1.2.2 Regelungsfolgenabschätzung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              pruefungeinhaltungderanforderungen123: {
                nameLink: '1.2.3 Prüfung: Einhaltung der Anforderungen',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            hausabstimmung13: {
              nameLink: '1.3 Hausabstimmung',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              verfahrenaufarbeitsundstaatssekretaersebene131: {
                nameLink: '1.3.1 Verfahren auf Arbeits- und Staatssekretärsebene',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              entscheidunghausleitung132: {
                nameLink: '1.3.2 Entscheidung Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            ressortabstimmungundweiterebeteiligungen14: {
              nameLink: '1.4 Ressortabstimmung und weitere Beteiligungen',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              ressortabstimmung141: {
                nameLink: '1.4.1 Ressortabstimmung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              beteiligungnkr142: {
                nameLink: '1.4.2 Beteiligung NKR',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              eroerterungderaenderungswuensche143: {
                nameLink: '1.4.3 Erörterung der Änderungswünsche',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              erneuteabstimmungimressort144: {
                nameLink: '1.4.4 Erneute Abstimmung im Ressort',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            laender15: {
              nameLink: '1.5 Länder- und Verbändebeteiligung',
            },
            kabinettbeschlussoderbilligungdurchminister16: {
              nameLink: '1.6 Kabinettbeschluss oder Billigung durch Minister',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              schlussabstimmung161: {
                nameLink: '1.6.1 Schlussabstimmung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettvorlageanhausleitung162: {
                nameLink: '1.6.2 Kabinettvorlage an Hausleitung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettvorlageanbundeskanzleramt163: {
                nameLink: '1.6.3 Kabinettvorlage an Bundeskanzleramt',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              kabinettbeschluss164: {
                nameLink: '1.6.4 Kabinettbeschluss',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
            bundestagbundestag17: {
              nameLink: '1.7 Befassung Bundestag',
            },
            bundesrat18: {
              nameLink: '1.8 Befassung Bundesrat',
            },
            bundesregierung19: {
              nameLink: '1.9 Befassung Bundesregierung Bundesminister nach Maßgabebeschluss',
            },
          },

          phaseTitle2: {
            nameLink: 'Phase 2 <br>Abschlussverfahren',
            ausfertigungundverkuendung21: {
              nameLink: '2.1 Ausfertigung und Verkündung',
              content: ``,
              linkedBibEntries: [],
              linkedModules: [],
              ausfertigung211: {
                nameLink: '2.1.1 Ausfertigung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              herstellungderurschrift212: {
                nameLink: '2.1.2 Herstellung der Urschrift',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
              verkuendung213: {
                nameLink: '2.1.3 Verkündung',
                content: ``,
                linkedBibEntries: [],
                linkedModules: [],
              },
            },
          },
        },
      },
      verwaltungsvorschrift: {
        nameLink: 'Verwaltungsvorschrift (zukünftig verfügbar)',
        allgemeineVerwaltungsvorschrift: {
          nameLink: 'Allgemeine Verwaltungsvorschrift',
          content: ``,
        },
      },
    },
    glossarContent: {
      a: {
        link: 'A',
        content: 'Content A',
      },
      b: {
        link: 'B',
        content:
          'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Amet, vitae numquam placeat sint eveniet iure officia necessitatibus ipsum quisquam minima in hic, dolores fuga totam consequatur aspernatur? Inventore, fugit velit! <br>',
      },
      c: {
        link: 'C',
        content: 'Content C',
      },
      d: {
        link: 'D',
        content: 'Content D',
      },
      e: {
        link: 'E',
        content: 'Content E',
      },
      f: {
        link: 'F',
        content: 'Content F',
      },
      g: {
        link: 'G',
        content: 'Content G',
      },
      h: {
        link: 'H',
        content: 'Content H',
      },
      i: {
        link: 'I',
        content: 'Content I',
      },
      j: {
        link: 'J',
        content: 'Content J',
      },
      k: {
        link: 'K',
        content: 'Content K',
      },
      l: {
        link: 'L',
        content: 'Content L',
      },
      m: {
        link: 'M',
        content: 'Content M',
      },
      n: {
        link: 'N',
        content: 'Content N',
      },
      o: {
        link: 'O',
        content: 'Content O',
      },
      p: {
        link: 'P',
        content: 'Content P',
      },
      q: {
        link: 'Q',
        content: 'Content Q',
      },
      r: {
        link: 'R',
        content: 'Content R',
      },
      s: {
        link: 'S',
        content: 'Content S',
      },
      t: {
        link: 'T',
        content: 'Content T',
      },
      u: {
        link: 'U',
        content: 'Content U',
      },
      v: {
        link: 'V',
        content: 'Content V',
      },
      w: {
        link: 'W',
        content: 'Content W',
      },
      x: {
        link: 'X',
        content: 'Content X',
      },
      y: {
        link: 'Y',
        content: 'Content Y',
      },
      z: {
        link: 'Z',
        content: 'Content Z',
      },
      ae: {
        link: 'Ä',
        content: 'Content Ä',
      },
      oe: {
        link: 'Ö',
        content: 'Content Ö',
      },
      ue: {
        link: 'Ü',
        content: 'Content Ü',
      },
    },
  },
};
