// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { NavBarItem } from '@plateg/theme';

export interface MessagesType {
  [key: string]: string | MessagesType;
}

export class NavItemCreationController {
  private readonly ignoreList = new Set(['nameLink', 'content', 'linkedBibEntries', 'linkedModules']);

  public generateNavItems(navMessages: MessagesType, prefix = '', level = 0): NavBarItem[] {
    const navBarList: NavBarItem[] = [];
    if (typeof navMessages !== 'object') {
      return navBarList;
    }
    const children = this.getNavChildren(navMessages);
    let menuTitle = '';
    children.forEach((item) => {
      if (item.startsWith('phaseTitle')) {
        menuTitle = item;
      }
      const grandChildren = this.getNavChildren(navMessages[item]);
      if (menuTitle !== '') {
        navBarList.push({
          menuTitle: `${prefix}${menuTitle}`,
          submenuEntry: {
            key: ``,
          },
          menuKeys: this.generateNavItems(navMessages[item], `${prefix}${item}.`, level + 1),
        });
        menuTitle = '';
      } else if (grandChildren.length > 0) {
        navBarList.push({
          submenuEntry: {
            key: `${prefix}${item}`,
            isLink: true,
          },
          menuKeys: this.generateNavItems(navMessages[item], `${prefix}${item}.`, level + 1),
        });
      } else {
        navBarList.push({
          key: `${prefix}${item}`,
        });
      }
    });
    return navBarList;
  }

  private getNavChildren(entry: MessagesType): string[] {
    const children = Object.keys(entry);
    return children.filter((item) => !this.ignoreList.has(item));
  }
}
