// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { NavItemCreationController } from './controller';
describe('TEST: generateNavItems', () => {
  const ctrl = new NavItemCreationController();
  it('Simple menu, no submenus', () => {
    const messages = { a: { nameLink: 'First Entry' }, b: { nameLink: 'Second Entry' } };
    const expectedResult = [{ key: 'a' }, { key: 'b' }];
    expect(ctrl.generateNavItems(messages)).to.eql(expectedResult);
  });
  it('Simple submenu', () => {
    const messages = {
      a: { nameLink: 'First Entry', b: { nameLink: 'Second SubEntry' }, c: { nameLink: 'Second SubEntry' } },
    };
    const expectedResult = [
      {
        submenuEntry: {
          key: 'a',
          isLink: true,
        },
        menuKeys: [{ key: 'a.b' }, { key: 'a.c' }],
      },
    ];
    expect(ctrl.generateNavItems(messages)).to.eql(expectedResult);
  });
  it('Complex submenu', () => {
    const messages = {
      a: {
        nameLink: 'First Entry',
        phaseTitle1: { nameLink: 'First Titel' },
        b: { nameLink: 'Second SubEntry', c: { nameLink: 'Second SubEntry' } },
      },
    };
    const expectedResult = [
      {
        submenuEntry: {
          key: 'a',
          isLink: true,
        },
        menuKeys: [
          {
            menuTitle: 'a.phaseTitle1',
            submenuEntry: {
              key: '',
            },
            menuKeys: [],
          },
          {
            submenuEntry: {
              key: 'a.b',
              isLink: true,
            },
            menuKeys: [
              {
                key: 'a.b.c',
              },
            ],
          },
        ],
      },
    ];

    expect(ctrl.generateNavItems(messages)).to.eql(expectedResult);
  });
});
