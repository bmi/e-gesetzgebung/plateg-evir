// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Affix, Layout, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  GlobalDI,
  HeaderComponent,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
  MenuCollapseController,
  NavbarComponent,
  SiderWrapper,
} from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';

import { routes } from '../../shares/routes';
import { MainComponent } from './main/component.react';
import { EVIRGlossarComponent } from './main/glossar/component.react';
import { NavItemCreationController } from './nav/controller';

export function EVIRHomeBreadcrumb(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <Link id="evir-home-link" key="evir-home" to={`/${routes.EVIR}`}>
      {t('evir.header.linkHome')}
    </Link>
  );
}

export function EVIRGlossarHeader(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <InfoComponent
      title={t('evir.glossar.drawerTitle')}
      buttonText={t('evir.header.linkGlossar')}
      buttonId="glossar-button"
      prefilledWidth={1104}
      titleWithoutPrefix={true}
    >
      <EVIRGlossarComponent />
    </InfoComponent>
  );
}

export function HomeTextLabel(): React.ReactElement {
  const { t } = useTranslation();
  return <span key="evir-home">{t('evir.header.linkHome')}</span>;
}

const { Header, Content } = Layout;

export function EVIR(): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);
  const headerController = GlobalDI.get<HeaderController>('headerController');
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const [menuOffsetTop, setMenuOffsetTop] = useState<number>(0);
  const menuCollapseController = new MenuCollapseController(isCollapsed, setIsCollapsed);
  useEffect(() => {
    headerController.setHeaderProps({
      setMenuOffset: setMenuOffsetTop,
    });
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error) => {
        console.log('Error sub', error);
      },
    });
    loadingStatusController.initLoadingStatus();

    return () => subs.unsubscribe();
  }, []);

  useEffect(() => {
    menuCollapseController.registerListener();
    return () => menuCollapseController.removeListener();
  }, []);

  useEffect(() => {
    menuCollapseController.updateButtonPosition();
  });

  useEffect(() => {
    menuCollapseController.configureCollapseButton();
  }, [isCollapsed]);

  const customTranslation = (name: string) => {
    return t(`evir.nav.${name}.nameLink`);
  };

  const generateNavBarItems = () => {
    const navMessages = t('evir.nav', { returnObjects: true });
    const ctrl = new NavItemCreationController();
    return ctrl.generateNavItems(navMessages);
  };

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{
          height: '100%',
          display: loadingStatus === true ? 'none' : 'flex',
        }}
        className="site-layout-background"
      >
        <Header className="header-component site-layout-background" id="evir-header">
          <HeaderComponent ctrl={headerController} />
        </Header>
        <Layout className="has-drawer ant-layout-has-sider">
          <SiderWrapper
            breakpoint="lg"
            collapsedWidth="30"
            onCollapse={() => setIsCollapsed(!isCollapsed)}
            collapsible
            width={280}
            collapsed={isCollapsed}
            className="navbar-component evir-navbar-component"
          >
            <Affix offsetTop={menuOffsetTop}>
              {!isCollapsed && <NavbarComponent navBarItems={generateNavBarItems()} trans={customTranslation} />}
            </Affix>
          </SiderWrapper>
          <Layout className="main-content-area has-drawer site-layout-background">
            <Content style={{ padding: '24px' }}>
              <MainComponent />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </Spin>
  );
}
