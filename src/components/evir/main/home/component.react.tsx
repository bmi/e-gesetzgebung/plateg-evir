// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { BreadcrumbComponent, GlobalDI, HeaderController } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EVIRGlossarHeader, EVIRHomeBreadcrumb, HomeTextLabel } from '../../component.react';

export function EVIRHomeComponent(): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();

  const headerController = GlobalDI.get<HeaderController>('headerController');
  const routeMatcherVorbereitung = useRouteMatch<{ level1: string }>(`/${routes.EVIR}/:level1`);

  const setBreadcrumb = (activeSubmenu: string | undefined) => {
    const breadcrumbsItems = [];
    if (activeSubmenu) {
      breadcrumbsItems.push(<EVIRHomeBreadcrumb key="evir-header-home" />);
      breadcrumbsItems.push(<span key={`evir-${activeSubmenu}`}>{t(`evir.nav.${activeSubmenu}.nameLink`)}</span>);
    } else {
      breadcrumbsItems.push(<HomeTextLabel key="evir-header-home" />);
    }

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={breadcrumbsItems} />],
      headerLast: [<EVIRGlossarHeader key="evir-header-glossar" />],
    });
  };

  useEffect(() => {
    setBreadcrumb(routeMatcherVorbereitung?.params.level1);
    window.scrollTo(0, 0);
  }, [location, routeMatcherVorbereitung?.params.level1]);

  return (
    <Row>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 14, offset: 2 }}
        lg={{ span: 16, offset: 3 }}
        xl={{ span: 12, offset: 3 }}
        xxl={{ span: 10, offset: 4 }}
      >
        <div className="evir-home-page">
          <h1 className="ant-typography">{t(`evir.home.title`)}</h1>
          <p>{t(`evir.home.text1`)}</p>
          <p>{t(`evir.home.text2`)}</p>
          <h3 className="ant-typography">{t(`evir.home.titleVerfahrensassistentGesetze`)}</h3>
          <ul className="evir-links-list">
            <li>
              <Button
                id="evir-home-linkRegierungsinitiative"
                type="link"
                onClick={() => history.push(`/${routes.EVIR}/gesetz/regierungsinitiative`)}
              >
                {t(`evir.home.linkRegierungsinitiative`)}
              </Button>
            </li>
          </ul>

          <h3 className="ant-typography">{t(`evir.home.titleVerfahrensassistentRechtsverordnungen`)}</h3>
          <ul className="evir-links-list">
            <li>
              <Button
                id="evir-home-linkRechtsverordnung"
                type="link"
                onClick={() =>
                  history.push(
                    `/${routes.EVIR}/rechtsverordnung/rechtsverordnungDerBundesregierungOderEinesOderMehrererBundesminister`,
                  )
                }
              >
                {t(`evir.home.linkRechtsverordnung`)}
              </Button>
            </li>
          </ul>

          <h3 className="ant-typography">{t(`evir.home.titleVerfahrensassistentVerwaltungsvorschriften`)}</h3>
          <ul className="evir-links-list">
            <li>
              <Button
                id="evir-allgemeineVerwaltungsvorschrift-btn"
                type="link"
                onClick={() => history.push(`/${routes.EVIR}/verwaltungsvorschrift/verwaltungsvorschrift`)}
              >
                {t(`evir.home.linkVerwaltungsvorschrift`)}
              </Button>
            </li>
          </ul>
        </div>
      </Col>
    </Row>
  );
}
