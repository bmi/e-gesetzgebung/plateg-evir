// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { WorkHelpEntry } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  ErrorController,
  GlobalDI,
  HeaderController,
  LeftOutlined,
  LoadingStatusController,
  RightOutlined,
  WorkingAidsController,
} from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EVIRGlossarHeader, EVIRHomeBreadcrumb } from '../../component.react';
import EVIRGlossarEntriesContainingArea from '../glossar/glossar-entries-containing-area/component.react';
import { PageController } from './controller';
import { LinkedBibEntry } from './linked-bib-entry/component.react';
import { LinkedModuleEntry } from './linked-module-entry/component.react';

export function PageComponent(): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;

  const history = useHistory();
  const location = useLocation();

  const headerController = GlobalDI.get<HeaderController>('headerController');
  const pageController = GlobalDI.getOrRegister('pageController', () => new PageController());
  const ctrl = GlobalDI.getOrRegister('workingAidsController', () => new WorkingAidsController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');

  const [title, setTitle] = useState('');
  const [pageContent, setPageContent] = useState<React.ReactElement>();
  const [marginalColumn, setMarginalColumn] = useState<React.ReactElement>();
  const [surroundingPages, setSurroundingPageKeysPages] = useState<{
    before?: string;
    after?: string;
  }>();
  const [workHelpEntries, setworkHelpEntries] = useState<WorkHelpEntry[]>();

  useEffect(() => {
    const translationKey = location.pathname.replace(`/${routes.EVIR}/`, '').replace(/\//g, '.');
    const basePath = (() => {
      const path = translationKey.slice(0, translationKey.indexOf('.', translationKey.indexOf('.') + 1));
      return path.length ? path : translationKey;
    })();

    setTitle(t(`evir.nav.${translationKey}.nameLink`));
    setPageContent(
      <div
        dangerouslySetInnerHTML={{
          __html: t(`evir.nav.${translationKey}.content`),
        }}
      ></div>,
    );
    const linkedModules: string[] = t(`evir.nav.${translationKey}.linkedModules`, { returnObjects: true });
    const linkedBibEntries: string[] = t(`evir.nav.${translationKey}.linkedBibEntries`, { returnObjects: true });
    setMarginalColumn(generateMarginalColumn(linkedModules, linkedBibEntries));
    setBreadcrumb(translationKey);
    setSurroundingPageKeysPages(pageController.findSurroundingPages(translationKey, basePath));

    window.scrollTo(0, 0);
  }, [location, workHelpEntries]);

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);

    const sub = ctrl.getArbeitshilfenListCall().subscribe({
      next: (workHelpEntries: WorkHelpEntry[]) => {
        loadingStatusController.setLoadingStatus(false);
        setworkHelpEntries(workHelpEntries);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'workingAids.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => sub.unsubscribe();
  }, []);

  const setBreadcrumb = (translationKey: string) => {
    const lastItem = <span key={`evir-${translationKey}`}>{t(`evir.nav.${translationKey}.nameLink`)}</span>;
    const listKeys = translationKey.split('.');
    const keysArray: React.ReactElement[] = [];
    // Remove last as it should not be a link
    listKeys.pop();

    // Prepare links for breadcrumbs
    let joinedKey = '';
    listKeys.forEach((item) => {
      joinedKey = joinedKey ? `${joinedKey}.${item}` : item;
      const joinedKeyUrl = joinedKey.replace(/\./g, '/');
      // Dont add link to breadcrumbs for phase title
      if (!item.startsWith('phaseTitle')) {
        keysArray.push(
          <Link id={`evir-${joinedKey}-link`} key={`evir-${joinedKey}`} to={`/${routes.EVIR}/${joinedKeyUrl}`}>
            {t(`evir.nav.${joinedKey}.nameLink`)}
          </Link>,
        );
      }
    });

    // Add last item to breadcrumbs
    keysArray.push(lastItem);

    headerController.setHeaderProps({
      headerLeft: [
        <BreadcrumbComponent key="breadcrumb" items={[<EVIRHomeBreadcrumb key="evir-header-home" />, ...keysArray]} />,
      ],
      headerLast: [<EVIRGlossarHeader key="evir-header-glossar" />],
    });
  };

  const generateMarginalColumn = (linkedModules: string[], linkedBibEntries: string[]): React.ReactElement => {
    return (
      <>
        {typeof linkedModules === 'object' && linkedModules?.length > 0 && (
          <div style={{ marginBottom: '60px' }}>
            <Title level={2}>{t('evir.marginalSpalte.applicationsTitle')}</Title>
            <hr />
            {linkedModules?.map((moduleName) => <LinkedModuleEntry key={moduleName} moduleName={moduleName} />)}
          </div>
        )}
        {typeof linkedBibEntries === 'object' && linkedBibEntries?.length > 0 && (
          <div>
            <Title level={2}>{t('evir.marginalSpalte.bibEntriesTitle')}</Title>
            <hr />
            {linkedBibEntries?.map((entryName) => {
              const bibEntry = workHelpEntries?.find((item) => item.title === entryName);
              return <LinkedBibEntry key={entryName} bibEntry={bibEntry} />;
            })}
          </div>
        )}
      </>
    );
  };

  const columnOptions = {
    xs: { span: 22, offset: 1 },
    md: { span: 14, offset: 2 },
    lg: { span: 16, offset: 3 },
    xl: { span: 12, offset: 3 },
    xxl: { span: 10, offset: 4 },
  };

  return (
    <EVIRGlossarEntriesContainingArea>
      <div className="evir-page" key={`evir-page-${title}`}>
        <Row>
          <Col {...columnOptions}>
            <Title level={1}>{title}</Title>
            {pageContent}
          </Col>
          <Col
            xs={{ span: 15, offset: 1 }}
            md={{ span: 12, offset: 2 }}
            lg={{ span: 10, offset: 3 }}
            xl={{ span: 7, offset: 1 }}
            xxl={{ span: 5, offset: 2 }}
          >
            <div>{marginalColumn}</div>
          </Col>
        </Row>
        <Row>
          <Col {...columnOptions}>
            <div className="form-control-buttons">
              {surroundingPages?.before && (
                <Button
                  id="evir-vorherigerSchritt-btn"
                  type="text"
                  size={'large'}
                  className="btn-prev"
                  onClick={() => history.push(`/${routes.EVIR}/${surroundingPages.before as string}`)}
                >
                  <LeftOutlined /> Vorheriger Schritt
                </Button>
              )}
              {surroundingPages?.after && (
                <Button
                  id="evir-naechsterSchritt-btn"
                  type="text"
                  size={'large'}
                  className="btn-next"
                  onClick={() => history.push(`/${routes.EVIR}/${surroundingPages.after as string}`)}
                >
                  Nächster Schritt <RightOutlined />
                </Button>
              )}
            </div>
          </Col>
        </Row>
      </div>
    </EVIRGlossarEntriesContainingArea>
  );
}
