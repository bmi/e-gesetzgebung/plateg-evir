// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { WorkHelpEntry, WorkHelpEntryFormatEnum } from '@plateg/rest-api';

interface LinkedModuleEntryProps {
  bibEntry?: WorkHelpEntry;
}
export function LinkedBibEntry(props: LinkedModuleEntryProps): React.ReactElement {
  const { bibEntry } = props;

  const prepareDocumentType = (format: string) => {
    let formatLabel = '';
    switch (format) {
      case WorkHelpEntryFormatEnum.Pdf.toString():
        formatLabel = `(${format})`;
        break;
      case WorkHelpEntryFormatEnum.Html.toString():
        formatLabel = `(HTML)`;
        break;
      case WorkHelpEntryFormatEnum.Docx.toString():
        formatLabel = `(Word)`;
        break;
      case WorkHelpEntryFormatEnum.Xlsm.toString():
        formatLabel = `(Excel)`;
        break;
    }
    return formatLabel;
  };

  return (
    <div className="outer-div">
      {bibEntry && (
        <div className="ant-typography">
          <a
            id={`evir-arbeitshilfe-link-${bibEntry.title.toLowerCase().split(' ').join('-')}`}
            href={bibEntry.url}
            target="_blank"
            className={`module-link ${bibEntry.format !== WorkHelpEntryFormatEnum.Html ? 'internal-link' : ''}`}
          >
            {bibEntry.title} {bibEntry.format && prepareDocumentType(bibEntry.format)}
          </a>
          <div className="module-description">{bibEntry.description}</div>
        </div>
      )}
    </div>
  );
}
