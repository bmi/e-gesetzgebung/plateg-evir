// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './linked-module-entry.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  ArbeitshilfenbibliothekMenuIcon,
  EditorMenuIcon,
  GesetzesfolgenabschaetzungMenuIcon,
  HRAMenuIcon,
  PKPMenuIcon,
  RVMenuIcon,
  VorbereitungMenuIcon,
  ZeitMenuIcon,
} from '@plateg/theme';

interface LinkedModuleEntryProps {
  moduleName: string;
}
interface ModuleMapper {
  [key: string]: { title: string; link: string; description: string };
}

const getModuleIcons = (module: string) => {
  const styleIcons = { height: 30, width: 30 };
  switch (module) {
    case 'zeitplanung':
      return <ZeitMenuIcon style={styleIcons} />;
    case 'regelungsvorhaben':
      return <RVMenuIcon style={styleIcons} />;
    case 'abstimmung':
      return <HRAMenuIcon style={styleIcons} />;
    case 'editor':
      return <EditorMenuIcon style={styleIcons} />;
    case 'vorbereitung':
      return <VorbereitungMenuIcon style={styleIcons} />;
    case 'gesetzesfolgenabschätzung':
      return <GesetzesfolgenabschaetzungMenuIcon style={styleIcons} />;
    case 'arbeitshilfenbibliothek':
      return <ArbeitshilfenbibliothekMenuIcon style={styleIcons} />;
    case 'pkp':
      return <PKPMenuIcon style={styleIcons} />;
    default:
      return <></>;
  }
};

export function LinkedModuleEntry(props: LinkedModuleEntryProps): React.ReactElement {
  const { t } = useTranslation();
  const [moduleMapping, setModuleMapping] = useState<ModuleMapper>();
  const [linkName, setLinkName] = useState<string>('');

  useEffect(() => {
    setModuleMapping(t('evir.modules', { returnObjects: true }));
  }, []);

  useEffect(() => {
    setLinkName(moduleMapping?.[props.moduleName]?.title || '');
  }, [moduleMapping]);

  return (
    <div className="outer-div">
      <a
        id={`evir-linkedModuleEntry-icon-link-${linkName.toLowerCase().split(' ').join('-')}`}
        href={moduleMapping?.[props.moduleName]?.link}
        className="module-icon"
      >
        {getModuleIcons(props.moduleName)}
      </a>
      <div>
        <a
          id={`evir-linkedModuleEntry-link-${linkName.toLowerCase().split(' ').join('-')}`}
          href={moduleMapping?.[props.moduleName]?.link}
          className="module-link"
        >
          {linkName}
        </a>
        <div className="module-description">{moduleMapping?.[props.moduleName]?.description}</div>
      </div>
      <br />
    </div>
  );
}
