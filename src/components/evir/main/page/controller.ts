// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

import { MenuItem, NavBarItem } from '@plateg/theme';

import { MessagesType, NavItemCreationController } from '../../nav/controller';
export type NavBarType = NavBarItem | MenuItem;
export class PageController {
  private keyList: string[] = [];

  public constructor(customNavMessages?: MessagesType) {
    const flatNavBarItem = (item: NavBarItem): string[] => {
      const key = item.submenuEntry?.key && item.submenuEntry?.isLink ? item.submenuEntry.key : undefined;
      const menuKey = item.menuKeys?.flatMap((menuKeyItem) =>
        menuKeyItem.hasOwnProperty('key') ? (menuKeyItem as MenuItem).key : flatNavBarItem(menuKeyItem as NavBarItem),
      );
      if (key) {
        (menuKey ?? []).unshift(key);
      }
      return menuKey;
    };
    const navMessages = customNavMessages || i18n.t('evir.nav', { returnObjects: true });
    const ctrl = new NavItemCreationController();
    const navBarItems = ctrl.generateNavItems(navMessages);
    this.keyList = navBarItems.flatMap(flatNavBarItem);
  }

  public findSurroundingPages(key: string, basePath: string): { before?: string; after?: string } {
    const index = this.keyList.findIndex((keyListEntry) => {
      return keyListEntry === key;
    });
    return { before: this.findPredecessor(index, basePath), after: this.findSuccessor(index, basePath) };
  }

  private findPredecessor(index: number, basePath: string): string | undefined {
    if (index <= 0) {
      return undefined;
    } else {
      const predecessor = this.keyList[index - 1];
      return (
        (predecessor?.startsWith(basePath) ? this.getTransformedKey(predecessor) : undefined) ||
        this.findPredecessor(index - 1, basePath) ||
        undefined
      );
    }
  }

  private findSuccessor(index: number, basePath: string): string | undefined {
    if (this.keyList.length < index) {
      return undefined;
    } else {
      const successor = this.keyList[index + 1];
      return (
        (successor?.startsWith(basePath) ? this.getTransformedKey(successor) : undefined) ||
        this.findSuccessor(index + 1, basePath) ||
        undefined
      );
    }
  }

  private getTransformedKey(key: string): string {
    return key.replace(/\./g, '/');
  }
}
