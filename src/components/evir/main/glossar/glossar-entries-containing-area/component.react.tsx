// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useLayoutEffect } from 'react';

export default function EVIRGlossarEntriesContainingArea(props: { children: React.ReactNode }): React.ReactElement {
  const openGlossarEntry = async (keyword: string, dataTitle: string | null): Promise<void> => {
    const glossarButton = document.getElementById('glossar-button');
    if (!glossarButton?.classList.contains('active')) {
      glossarButton?.click();
    }

    await new Promise((resolve) => setTimeout(resolve, 20));
    const keywordCharButton = document.getElementById(`evir.glossarContent.${(dataTitle || keyword)[0].toLowerCase()}`);
    keywordCharButton?.click();

    await new Promise((resolve) => setTimeout(resolve, 20));
    const targetElement = document.querySelector(`[data-title='${dataTitle ?? keyword}']`);
    const glossarHolder = document.getElementById('glossar-content-holder');
    if (targetElement && glossarHolder) {
      glossarHolder.scroll(0, targetElement.getBoundingClientRect().y - 170);
    }
  };

  useLayoutEffect(() => {
    const glossarLinks = document.getElementsByClassName('glossar-link');
    Array.prototype.forEach.call(glossarLinks, (item: HTMLElement) => {
      item.onclick = function () {
        void openGlossarEntry(item.innerText, item.getAttribute('glossar-data-title'));
      };
      item.addEventListener('keydown', function (event) {
        if (event.key === 'Enter') {
          void openGlossarEntry(item.innerText, item.getAttribute('glossar-data-title'));
        }
      });
      item.tabIndex = 0;
    });
  });

  return <>{props.children}</>;
}
