// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/**
 * Glossar component
 * Show in drawer two columns:
 * 1. List of letters.
 * 2. Content corresponding to the selected letter.
 * @returns React.ReactElement
 */
import './glossar.less';

import { Col, Menu, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import EVIRGlossarEntriesContainingArea from './glossar-entries-containing-area/component.react';

interface GlossarInterface {
  [s: string]: GlossarItemInterface;
}
interface GlossarItemInterface {
  link: string;
  content: string;
  abbr: string;
}

export function EVIRGlossarComponent(): React.ReactElement {
  const { t } = useTranslation();
  const [glossarContentArea, setGlossarContentArea] = useState<React.ReactElement>();
  const [glossarList, setGlossarList] = useState<GlossarItemInterface[]>([]);

  /** Get data from translations as an Object */
  const glossarContent: GlossarInterface = t('evir.glossarContent', { returnObjects: true });
  const extentedContent: GlossarItemInterface[] = Object.keys(glossarContent).map((item) => {
    return { ...glossarContent[`${item}`], abbr: item };
  });
  const defaultKey = `evir.glossarContent.${extentedContent[0].abbr}`;
  const glossarContentHolder = document.getElementById('glossar-content-holder');

  useEffect(() => {
    setGlossarList(extentedContent);
    setGlossarContentArea(
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${defaultKey}.content`),
        }}
      ></div>,
    );
  }, []);

  /**
   * Handle click on letter and set glossar content area with corresponding text
   * @param key: string
   */
  const handleLetterClick = (key: string) => {
    if (glossarContentHolder) {
      glossarContentHolder.scrollTop = 0;
    }
    setGlossarContentArea(
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${key}.content`),
        }}
      ></div>,
    );
  };

  return (
    <EVIRGlossarEntriesContainingArea>
      <div className="glossar-wrapper">
        <Row className="glossar-holder">
          <Col xs={{ span: 4 }} className="menu-holder">
            <nav className="navbar-component" aria-label="Glossarnavigation">
              <Menu onClick={(item) => handleLetterClick(item.key)} defaultSelectedKeys={[`${defaultKey}`]}>
                {glossarList.map((item) => {
                  return (
                    <Menu.Item key={`evir.glossarContent.${item.abbr}`} id={`evir.glossarContent.${item.abbr}`}>
                      <button
                        id="evir-glossaerContent-btn"
                        title={item.link}
                        aria-label={item.link}
                        type="button"
                        className="ant-btn ant-btn-text"
                      >
                        <span>{item.link}</span>
                      </button>
                    </Menu.Item>
                  );
                })}
              </Menu>
            </nav>
          </Col>
          <Col xs={{ span: 20 }} className="content-holder" id="glossar-content-holder">
            {glossarContentArea}
          </Col>
        </Row>
      </div>
    </EVIRGlossarEntriesContainingArea>
  );
}
