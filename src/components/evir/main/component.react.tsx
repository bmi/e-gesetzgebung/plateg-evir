// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { routes } from '../../../shares/routes';
import { EVIRHomeComponent } from './home/component.react';
import { PageComponent } from './page/component.react';

export function MainComponent(): React.ReactElement {
  return (
    <Switch>
      <Route exact path={[`/${routes.EVIR}`, `/${routes.EVIR}/:level1`]}>
        <EVIRHomeComponent />
      </Route>

      <Route
        path={[
          `/${routes.EVIR}/:level1/:level2/`,
          `/${routes.EVIR}/:level1/:level2/:level3/`,
          `/${routes.EVIR}/:level1/:level2/:level3/:level4`,
        ]}
      >
        <PageComponent />
      </Route>
    </Switch>
  );
}
