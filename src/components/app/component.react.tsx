// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-evir.less';

import React from 'react';
import { Route, Switch } from 'react-router';

import { ArbeitshilfenControllerApi, CmsControllerApi, Configuration } from '@plateg/rest-api';
import { configureRestApi, GlobalDI, HeaderController, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../shares/routes';
import { EVIR } from '../evir/component.react';

export function EVIRApp(): React.ReactElement {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route path={`/${routes.EVIR}`}>
        <EVIR />
      </Route>
    </Switch>
  );
}
function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('headerController', () => new HeaderController());
  GlobalDI.getOrRegister('arbeitshilfenController', () => new ArbeitshilfenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('cmsController', () => new CmsControllerApi(configRestCalls));
}
