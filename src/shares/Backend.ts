// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AppType, CmsControllerApi, Configuration } from '@plateg/rest-api';
import { getMiddleware, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { MessagesType } from '../components/evir/nav/controller';
import { messages } from '../messages';

class Backend {
  public read(language: string, namespace: string, callback: (firstArg: null, messages: MessagesType) => void): void {
    const configRestCalls = new Configuration({ middleware: getMiddleware() });
    const cms_controller = GlobalDI.getOrRegister('cmsController', () => new CmsControllerApi(configRestCalls));
    const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
    loadingStatusCtrl.setLoadingStatus(true);
    cms_controller.getCmsText({ appType: AppType.Evir }).subscribe({
      next: (messageFilestream) => {
        void messageFilestream.text().then((text) => {
          const serverMessages = JSON.parse(text) as MessagesType;
          callback(null, { ...serverMessages['de']['translation'], ...messages['de']['translation'] } as MessagesType);
          loadingStatusCtrl.setLoadingStatus(false);
        });
      },
      error: (error) => {
        console.error('Could not load messages from backend', error);
        loadingStatusCtrl.setLoadingStatus(false);
      },
    });
  }
}
Backend.type = 'backend';
export default Backend;
